# README #

This repo contains the FreeRTOS build for 32-bit RISC-V toolchain.

### To Compile ###

* Go to Demo/riscv-spike
* In the Makefile change the RISCV variable (on line 61) to point to your riscv binary folder.
* Type make spike
* This will generate the hex, dump file, asm file and the excutable for FreeRTOS to be run on spike
* To create the hex and executables to be used on SHAKTI cores type - make shakti

### To run on Spike ###
* spike --isa=rv32g riscv-spike.elf
* A succesfull run on spike will display:

	PASS!, Instructions retired: 0, 11025971

### To run on SHAKTI
* you will need two files - riscv-spike.hex and config_string.hex to be loaded in the respective address spaces.

